# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 17:05:54 2020

@author: Vinod
"""

import numpy as np
import os
from datetime import datetime, timedelta
from netCDF4 import Dataset
import yaml
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from plot_tools import map_prop
from mod_colormap import add_white


def check_season(month_str):
    for season, months in seasons.items():
        if month_str in months:
            return season


# new_cm = add_white('Reds', shrink_colormap=True, replace_frac=0.1, start=None)
new_cm = add_white("jet", shrink_colormap=False, replace_frac=0.1,
                   start="min")

with open('config/config_roi.cnf') as conf:
    rois = yaml.safe_load(conf)
for roi in rois:
    rois[roi]['extent'] = rois[roi]['lonrange'].copy()
    rois[roi]['extent'].extend(rois[roi]['latrange'])
    rois[roi]['border_res'] = '10m'
state_capitals = {"Amravati": [16.51, 80.51], "New Delhi": [28.61, 77.21],
                  "Itanagar": [27.08, 93.61], "Dispur": [26.14, 91.79],
                  "Patna": [25.59, 85.14], "Raipur": [21.25, 81.63],
                  "Panaji": [15.4, 73.8],  "Gandhinagar": [23.2, 72.64],
                  "Shimla": [31.1, 77.2], "Ranchi": [23.34, 85.3],
                  "Bengaluru": [12.97, 77.59],
                  "Thiruvananthapuram": [8.52, 76.93],
                  "Bhopal": [23.26, 77.41], "Mumbai": [19.08, 72.88],
                  "Imphal": [24.82, 93.94], "Shillong": [25.58, 91.89],
                  "Aizawl": [23.73, 92.72], "Kohima": [25.67, 94.11],
                  "Bhubaneswar": [20.30, 85.82], "Chandigarh": [30.73, 76.78],
                  "Jaipur": [26.91, 75.79], "Gangtok": [27.33, 88.61],
                  "Chennai": [13.08, 80.27], "Hyderabad": [17.38, 78.49],
                  "Agartala": [23.83, 91.29], "Lucknow": [26.85, 80.95],
                  "Dehradun": [30.32, 78.03], "Kolkata": [22.57, 88.36],
                  "Srinagar": [34.08, 74.80], "Leh": [34.15, 77.58]}
# %% load data
satdir = r'M:\nobackup\vinod\tropomi_raster\raw\NO2\mon_mean'
savedir = r'D:\postdoc\book_chap\Chap2'
keepdir = r'D:\postdoc\Sat\India_grid_mask\tropomi_l3'
seasons = {"Winter": ["01", "02"], "Pre Monsoon": ["03", "04", "05"],
           "Monsoon": ["06", "07", "08", "09"],
           "Post Monsoon": ["10", "11", "12"]}
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
          'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

start_date = datetime(2018, 2, 1)
end_date = datetime(2020, 1, 1)
num_mnth = (end_date.year - start_date.year) * 12
num_mnth += end_date.month - start_date.month
date_range = [(start_date+timedelta(days=31*i)).replace(day=1)
              for i in range(num_mnth+1)]
inp_files = [os.path.join(satdir, 'no2_' + period + '.nc')
             for period in map(lambda x: x.strftime('%Y%m'), date_range)]
vcd = {s: [] for s in seasons.keys()}
vcd_month = {str(s).zfill(2): [] for s in 1+np.arange(12)}
f_count = 0
for inp_file in inp_files:
    month_str = inp_file.split('.')[0][-2:]
    season = check_season(month_str)
    with Dataset(inp_file) as f:
        vcd_now = f.variables['vcd'][:]
        vcd_now[vcd_now == -999] = np.nan
        vcd[season].append(vcd_now)
        vcd_month[month_str].append(vcd_now)
with Dataset(inp_file) as f:
    lats = f.variables['lat'][:]
    lons = f.variables['lon'][:]

for season, vcd_now in vcd.items():
    vcd[season] = np.nanmean(np.asarray(vcd[season]), 0)
for month, vcd_now in vcd_month.items():
    vcd_month[month] = np.nanmean(np.asarray(vcd_month[month]), 0)
(_, _, keepfiles) = next(os.walk(keepdir))

keep = np.zeros((len(lats), len(lons)), dtype=bool)
st_group = {'North Indian Plain': ['Bihar', 'Punjab', 'Haryana', 'Uttar', 'NCT'],
            'Coal belt': ['Chhattisgarh', 'Jharkhand', 'Odisha'],
            'South India': ['Telangana', 'Tamil', 'Kerala', 'Karnataka',
                            'Andhra']}
keep = {keys: np.zeros((len(lats), len(lons)), dtype=bool)
        for keys in st_group.keys()}
keep['India'] = np.zeros((len(lats), len(lons)), dtype=bool)
for keepfile in keepfiles:
    state = keepfile.split('_')[-1][:-4]
    keep_now = np.loadtxt(os.path.join(keepdir, keepfile))
    keep_now = keep_now.copy().astype(bool)
    keep['India'] |= keep_now
    for group_now in st_group.keys():
        if state in st_group[group_now]:
            print(state, group_now)
            keep[group_now] |= keep_now
# %% Seasonal profile in different regions
fig, ax = plt.subplots(figsize=[13, 4])
pos = -0.06
for group_now, keep_now in keep.items():
    temp_mean = [np.nanmean(vals[keep_now]) for vals in vcd_month.values()]
    temp_mean = np.asarray(temp_mean)*1e13/1e15
    temp_std = [np.nanstd(vals[keep_now]) for vals in vcd_month.values()]
    temp_std = np.asarray(temp_std)*1e13/1e15
    ax.errorbar(pos+1+np.arange(12), temp_mean, yerr=temp_std,
                capsize=4, label=group_now)
    pos += 0.04
ax.legend(loc=[0.5, 0.7], prop={'size': 12})
ax.grid(alpha=0.3)
ax.set_xticks(1+np.arange(12))
ax.set_xticklabels(months)
ax.set_ylabel('NO$_2$ tropospheric VCD (10$^{15}$ molecules cm$^{-2}$)')
plt.tight_layout()
plt.savefig(os.path.join(savedir, 'seasonality_region.png'), format='png',
            dpi=300)

# %%
fig, ax = plt.subplots()
bins = np.arange(-1, 10, 0.5)
for season in seasons.keys():
    counts, bins = np.histogram(vcd[season][keep['India']]*1e13/1e15, bins)
    ax.plot(bins[1:], counts, '-+', label=season)
ax.legend(loc='upper right', prop={'size': 12})
ax.set_ylabel('Number of grids', size=12)
ax.set_xlabel('NO$_2$ tropospheric VCD (10$^{15}$ molecules cm$^{-2}$)',
              size=12)
ax.grid(alpha=0.2)
plt.tight_layout()
plt.savefig(os.path.join(savedir, 'frequeny_season.png'), format='png',
            dpi=300)
# %%
powerplants = {}
with open(os.path.join(savedir, 'Coal_power_plant.csv')) as f:
    for s in f.readlines()[1:]:
        if not s:
            continue  # empty line
        elif s.startswith('#'):
            continue  # retired pp
        else:
            pp_now = s.split(',')
            powerplants[pp_now[2]] = [float(pp_now[1]), float(pp_now[0])]

season = 'Winter'
for season, vcd_now in vcd.items():
    fig, ax = plt.subplots(figsize=rois[roi]['figsize'],
                           subplot_kw=dict(projection=ccrs.PlateCarree()))
    map2plot = map_prop(rois['India'])
    vcd_now[~keep['India']] = np.nan
    datadict = {'lat': lats,
                'lon': lons,
                'plotable': vcd_now*1e13/1e15,
                'vmin': -0.3,
                'vmax': 9,
                'label': '',
                'text': 'NO$_2$ Tropospheric VCD (10$^{15}$ molecules cm$^{-2}$)'}
    scat = map2plot.plot_map(datadict, ax, cmap=new_cm, mode='pcolormesh',
                             alpha=0.9, add_state_feature=True, stamen=8)
    for i, city in enumerate(state_capitals):
        ax.scatter(state_capitals[city][1], state_capitals[city][0],
                   facecolors='none', edgecolors='k')
    for i, name in enumerate(powerplants):
        ax.scatter(powerplants[name][1], powerplants[name][0], marker='^',
                   facecolors='none', edgecolors='k')
    cb = fig.colorbar(scat, ax=ax, orientation='vertical',
                       extend='max', fraction=0.02, pad=0.05)
    cb.set_label(datadict['text'], size=16)
    cb.minorticks_on()
    fig.canvas.draw()
    plt.tight_layout()
    plt.savefig(os.path.join(savedir, season+'.png'), format='png', dpi=300)
    plt.close()
