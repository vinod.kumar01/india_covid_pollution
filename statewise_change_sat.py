# -*- coding: utf-8 -*-
"""
Created on Fri May 22 01:12:58 2020

@author: Vinod
"""
# %% imports and definition
import numpy as np
import os
from netCDF4 import Dataset
import yaml
import matplotlib.pyplot as plt
import shapefile
import cartopy.crs as ccrs
from plot_tools import map_prop, ShapelyFeature, Reader
from config.states_dl import st_dl
from mod_colormap import value2color


def sortlist(target, ref):
    target = [target for _, target in sorted(zip(ref, target))]
    return(target)


plot_map = False
with open('config/config_roi.cnf') as conf:
    rois = yaml.load(conf)

for roi in rois:
    rois[roi]['extent'] = rois[roi]['lonrange'].copy()
    rois[roi]['extent'].extend(rois[roi]['latrange'])
    rois[roi]['border_res'] = '10m'

keepdir = r'D:\postdoc\Sat\India_grid_mask'
satdir = r"M:\nobackup\vinod\tropomi_raster\crop\gridded"
#satdir = r"M:\nobackup\vinod\MODIS_AOD\raw\l2_1km\India_covid\stitched\gridded\mean"
savedir = r'D:\postdoc\Collab\MOES\Plots'
keeplist = os.listdir(keepdir)
states = np.genfromtxt(r'D:\postdoc\Collab\MOES\state_order_sel.txt',
                       dtype='str')
#states = [i.split('.')[0].split('_')[-1] for i in keeplist]
tracer_name = 'no2'
phase = 'Phase 1'
tracer = {'AOD': ['AOD (550 nm)',[0, 1]],
          'no2': ['NO$_2$ TVCD (molecules cm$^{-2}$)', [0, 1e16]],
          'so2': ['SO$_2$ TVCD (molecules cm$^{-2}$)', [-1e16, 3e16]],
          'hcho': ['HCHO (molecules cm$^{-2}$)', [0, 2e16]]}
phase_dir = {'Phase 1': ['25.03.19-14.04.19', '25 Mar. - 14 Apr.'],
             'Phase 2': ['15.04.19-03.05.19', '15 Mar. - 03 May'],
             'Phase 3': ['04.05.19-17.05.19', '04 May - 17 Jun.']}

f1 = Dataset(os.path.join(satdir, tracer_name, 'India',
                          'In_{}_{}.nc'.format(tracer_name, phase_dir[phase][0])))
f2 = Dataset(os.path.join(satdir, tracer_name, 'India',
                          'In_{}_{}.nc'.format(tracer_name,
                                               phase_dir[phase][0].replace('19', '20'))))
lats = f1.variables['lat'][:]
lons = f1.variables['lon'][:]
llons, llats = np.meshgrid(lons, lats)

try:
    mean_2019 = f1.variables['mean'][:]
    std_2019 = f1.variables['std'][:]
    count_2019 = f1.variables['count'][:]
    mean_2020 = f2.variables['mean'][:]
    std_2020 = f2.variables['std'][:]
    count_2020 = f2.variables['count'][:]
except KeyError:
    mean_2019 = f1.variables['AOD_550'][:]
    std_2019 = f1.variables['AOD_550_std'][:]
    count_2019 = f1.variables['count_550'][:]
    mean_2020 = f2.variables['AOD_550'][:]
    std_2020 = f2.variables['AOD_550_std'][:]
    count_2020 = f2.variables['count_550'][:]
f1.close()
f2.close()
#state2procs = ['MP', 'MH', 'KA', 'KL', 'PB', 'RJ', 'AP', 'BH', 'TS', 'WB',
#               'DL', 'GJ', 'HR', 'TN', 'UP']
mean_stats = {'state': [], 'rel_diff': [], 'rel_RMSD': [], 'count': [],
              'mean_2019': [], 'mean_2020': []}
# %% processing to calculate difference
boxdata_diff = []
boxdata_2019_mean, boxdata_2020_mean = [], []
for state in states:
#    if state not in state2procs:
#        continue
    mean_stats['state'].append(state)
    keep = np.loadtxt(os.path.join(keepdir, 'keep_3km_'+state+'.txt'))
    keep = keep.copy().astype(bool)
    mean_2019_sel = mean_2019[keep].filled(np.nan)
    # outlier removal
    mean_2019_sel[mean_2019_sel > np.nanpercentile(mean_2019_sel, 99)] = np.nan
    mean_2019_sel[mean_2019_sel < np.nanpercentile(mean_2019_sel, 1)] = np.nan
    std_2019_sel = std_2019[keep].filled(np.nan)
    count_2019_sel = count_2019[keep]
    mean_2020_sel = mean_2020[keep].filled(np.nan)
    # outlier removal
    mean_2020_sel[mean_2020_sel > np.nanpercentile(mean_2020_sel, 99)] = np.nan
    mean_2020_sel[mean_2020_sel < np.nanpercentile(mean_2020_sel, 1)] = np.nan
    std_2020_sel = std_2020[keep].filled(np.nan)
    count_2020_sel = count_2020[keep]
    # pixel wise difference
    diff = mean_2020_sel-mean_2019_sel
    outlierp = np.nanpercentile(diff, 99)
    outlierm = np.nanpercentile(diff, 1)
    diff[diff > outlierp] = np.nan
    diff[diff < outlierm] = np.nan
    count = len(diff[~np.isnan(diff)])
    std_2020_sel[diff > outlierp] = np.nan
    std_2020_sel[diff < outlierm] = np.nan
    # mean squared differnece
#    MSD = np.nanmean(std_2020_sel)**2 + np.nanmean(std_2019_sel)**2
    MSD = np.nanstd(mean_2020_sel)**2 + np.nanstd(mean_2019_sel)**2
#    std_2019_sel_comb = np.sqrt(np.nansum((std_2019_sel**2/count_2019_sel).filled(np.nan)))
#    std_2020_sel_comb = np.sqrt(np.nansum((std_2020_sel**2/count_2020_sel).filled(np.nan)))
#    MSD = np.nanmean(std_2020_sel_comb)**2 + np.nanmean(std_2019_sel_comb)**2
    RMSD = np.sqrt(MSD)
    data_frac = np.count_nonzero(~np.isnan(diff))/len(diff)
    rel_diff = 100*diff/mean_2019_sel
    boxdata_diff.append(rel_diff[~np.isnan(rel_diff)])
    boxdata_2019_mean.append(mean_2019_sel[~np.isnan(mean_2019_sel)])
    boxdata_2020_mean.append(mean_2020_sel[~np.isnan(mean_2020_sel)])
    if data_frac > 0.25:
#        rel_diff = 100*np.nanmean(diff)/np.nanmean(mean_2019_sel)
#        #difference of the mean
        rel_diff = np.nanmean(mean_2020_sel)-np.nanmean(mean_2019_sel)
        if np.nanmean(mean_2019_sel) < 0:
            rel_diff = np.nan
        rel_diff *= 100/np.nanmean(mean_2019_sel)
        rel_RMSD = 100*RMSD/np.nanmean(mean_2019_sel)
        mean_stats['rel_diff'].append(rel_diff)
        mean_stats['rel_RMSD'].append(rel_RMSD)
        mean_stats['count'].append(count)
    else:
#        rel_diff = 100*np.nanmean(diff)/np.nanmean(mean_2019_sel)
        rel_diff = np.nanmean(mean_2020_sel)-np.nanmean(mean_2019_sel)
        if np.nanmean(mean_2019_sel) < 0:
            rel_diff = np.nan
        rel_diff *= 100/np.nanmean(mean_2019_sel)
        mean_stats['rel_diff'].append(rel_diff)
        mean_stats['rel_RMSD'].append(np.nan)
        mean_stats['count'].append(count)
        print('less than 25% data for ' + state)
    if plot_map:
        llons_sel = llons[keep]
        llats_sel = llats[keep]
        map2plot = map_prop(rois['India'])
        datadict = {'lat': llats_sel, 'lon': llons_sel,
                    'plotable': mean_2019_sel,
                    'vmin': -3e14, 'vmax': 1.5e16, 'label': 'NO$_{2}$ VCD',
                    'text': 'NO$_2$ trop. VCD (molecules cm$^{-2}$)'}
        fig, ax = plt.subplots(figsize=rois[roi]['figsize'],
                               subplot_kw=dict(projection=ccrs.PlateCarree()))
        scat = map2plot.plot_map(datadict, ax, cmap='Reds', mode='scatter')
        cb = fig.colorbar(scat, ax=ax, orientation='horizontal',
                          extend='max', fraction=0.1, shrink=0.8, pad=0.1)
        cb.set_label(datadict['text'])
        plt.savefig(os.path.join(savedir, 'diff_{}_{}.png'.format(tracer_name,
                                 state)), format='png', dpi=300)
        plt.close()
fig1, ax1 = plt.subplots(figsize=[16, 4])
c = 'r'
bw = {'boxprops': {'color': c}, 'whiskerprops': {'color': c},
      'capprops': {'color': c}, 'medianprops': {'color': c},
      'meanprops': {'marker': 'o', 'markeredgecolor': c,
                    'markerfacecolor': 'none', 'markersize': 4}}

#bp = ax1.boxplot(boxdata_diff,
#                 positions=np.arange(len(boxdata_diff))-0.17,
#                 showfliers=False, showmeans=True, widths=0.15,
#                 boxprops=boxprops, medianprops=medianprops,
#                 capprops=capprops,
#                 meanprops=meanprops, whiskerprops=whiskerprops)
bp1 = ax1.boxplot(boxdata_2019_mean,
                  positions=np.arange(len(boxdata_diff))-0.12,
                  showfliers=False, showmeans=True, widths=0.2,
                  boxprops=bw['boxprops'], medianprops=bw['medianprops'],
                  capprops=bw['capprops'],
                  meanprops=bw['meanprops'], whiskerprops=bw['whiskerprops'])
for key in bw:
    bw[key]['color'] = 'b'
    bw[key]['markeredgecolor'] = 'b'

bp2 = ax1.boxplot(boxdata_2020_mean,
                  positions=np.arange(len(boxdata_diff))+0.12,
                  showfliers=False, showmeans=True, widths=0.2,
                  boxprops=bw['boxprops'], medianprops=bw['medianprops'],
                  capprops=bw['capprops'],
                  meanprops=bw['meanprops'], whiskerprops=bw['whiskerprops'])
ax1.set_xticks(np.arange(len(boxdata_diff)))
ax1.set_xticklabels(mean_stats['state'])
ax1.grid(alpha=0.4, axis='y')
ax1.set_ylabel(tracer[tracer_name][0], size=14)
ax1.set_ylim(tracer[tracer_name][1])
for i, v in enumerate(mean_stats['rel_diff']):
    ax1.text(i-0.2, ax1.yaxis.get_ticklocs()[-2],
             '{:.1f}%'.format(v) if ~np.isnan(v) else '--',
             color='k', size=12)
ax1.text(0.03, 0.9, 'Lockdown {} ({})'.format(phase, phase_dir[phase][1]),
         size=12, transform=ax1.transAxes)
ax1.set_ylim(ax1.get_ylim())
ax1.scatter(1, -1e20, facecolor='none', edgecolor='r', label='2019')
ax1.scatter(1, -1e20, facecolor='none', edgecolor='b', label='2020')
ax1.legend(loc='upper right')
plt.tight_layout()
plt.savefig(os.path.join(savedir,
                         'compare_bw_{}_{}.png'.format(tracer_name,
                                     phase.replace(' ', '_'))),
            format='png', dpi=300)

#ax1.set_ylim(-100, 100)

#ax1.scatter(1, -1000, c='g', label='Phase 3')
# sort data
#for key in mean_stats:
#    if key != 'rel_diff':
#        mean_stats[key] = sortlist(mean_stats[key],
#                                   mean_stats['rel_diff'])
#mean_stats['rel_diff'] = sortlist(mean_stats['rel_diff'],
#                                  mean_stats['rel_diff'])
# %% plot statistics
#fig, ax = plt.subplots(figsize=[16, 5])
#mask = np.isnan(mean_stats['rel_RMSD'])
#mask |= np.isinf(mean_stats['rel_RMSD'])
#ax.scatter(mean_stats['state'], mean_stats['rel_diff'], s=40, c='k')
#ax.bar(np.array(mean_stats['state'])[~mask],
#       np.array(mean_stats['rel_RMSD'])[~mask],
#       bottom=np.array(mean_stats['rel_diff'])[~mask],
#       facecolor='orange', alpha=0.4)
#ax.bar(np.array(mean_stats['state'])[~mask],
#       -1*np.array(mean_stats['rel_RMSD'])[~mask],
#       bottom=np.array(mean_stats['rel_diff'])[~mask],
#       facecolor='orange', alpha=0.4)
#plt.axhline(0, c='r')
## avoid clipping in case nan are at end
#x = np.arange(len(mean_stats['state']))
#ax.set(xticks=x, xticklabels=mean_stats['state'])
#text_loc = np.array(mean_stats['rel_diff']) + np.array(mean_stats['rel_RMSD'])
#text_loc[mask] = 0
#for i, v in enumerate(zip(mean_stats['count'], text_loc)):
#    ax.text(i-0.35, v[1]+2, '{:.1f}'.format(v[0]/100), color='blue', size=8)
## ax.xaxis.set_tick_params(rotation=90)
#ax.grid(alpha=0.4, axis='y')
#ax.set_ylabel('Relative differnece (%)', size=14)
#ax.text(0.05, 0.85, phase+'\n'+tracer[tracer_name][0], transform=ax.transAxes)
#ax.set_xlim(-2, len(x))
#ax.set_ylim(-90, 90)
#ax.tick_params(axis='x', which='minor', bottom=False)
#ax.minorticks_on()
plt.tight_layout()
#plt.savefig(os.path.join(savedir,
#                         'Plot_diff_{}_{}.png'.format(tracer_name,
#                                                      phase.replace(' ',
#                                                                    '_'))),
#            format='png', dpi=300)
# %% plot statistics on a map

#fig, ax = plt.subplots(figsize=rois[roi]['figsize'],
#                       subplot_kw=dict(projection=ccrs.PlateCarree()))
#map2plot = map_prop(rois['India'])
#In_st_shp = r'D:\python_toolbox\Igismap\Indian_States.shp'
#shapeRecs = shapefile.Reader(In_st_shp).shapeRecords()
#st_shape = [i for i in Reader(In_st_shp).geometries()]
#datadict = {'lat': np.zeros(len(mean_stats['rel_diff'])),
#            'lon': np.zeros(len(mean_stats['rel_diff'])),
#            'plotable': mean_stats['rel_diff'],
#            'vmin': -50, 'vmax': 50,
#            'label': phase+'\n'+tracer[tracer_name][0],
#            'text': 'Relative change with respect to 2019 (%)'}
#scat = map2plot.plot_map(datadict, ax, cmap='RdBu_r', mode='scatter',
#                         alpha=0.9, add_state_feature=False)
#cb = fig.colorbar(scat, ax=ax, orientation='horizontal',
#                  extend='both', fraction=0.05, shrink=0.7, pad=0.07)
#cb.set_label(datadict['text'])
#cval = {}
#for state_code, rel_diff in zip(mean_stats['state'], mean_stats['rel_diff']):
#    if np.isnan(rel_diff):
#        cval[state_code] = "none"
#    else:
#        cval[state_code] = value2color(rel_diff, datadict['vmin'],
#                                       datadict['vmax'], 'RdBu_r')
#for i, st_shape in enumerate(Reader(In_st_shp).geometries()):
#    state_code = st_dl[shapeRecs[i].record[0]]
#    state_feature = ShapelyFeature(st_shape, crs=ccrs.PlateCarree(),
#                                   edgecolor='k', alpha=0.9)
#    ax.add_feature(state_feature, facecolor=cval[state_code])
#plt.savefig(os.path.join(savedir,
#                         'Map_diff_{}_{}.png'.format(tracer_name,
#                                                     phase.replace(' ', '_'))),
#            format='png', dpi=300)
