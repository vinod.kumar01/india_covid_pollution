# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 15:43:44 2020

@author: Vinod
"""
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io.img_tiles as cimgt
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import numpy as np

stamen_terrain = cimgt.Stamen('terrain-background')


class map_prop:
    def __init__(self, prop_dict):
        self.extent = prop_dict['extent']
        self.name = prop_dict['name']
        self.border_res = prop_dict['border_res']

    def plot_map(self, datadict, ax, **kwargs):
        '''
        @ax: axis to plot
        @datadict['legend_text']: legend text
        @datadict[label']: Data label
        @datadict['lon']: longitude
        @datadict['lat']: latitude
        @datadict['plotable']: quantity to plot
        @datadict[vmin]: lower limit
        @datadict[vmax]: upper limit
        '''
        cmap = kwargs.get('cmap', 'jet')
        mode = kwargs.get('mode', 'pcolormesh')
        add_state_feature = kwargs.get('add_state_feature', True)
        alpha = kwargs.get('alpha', 0.5)
        projection = kwargs.get('projection', ccrs.PlateCarree())
        stamen = kwargs.get('stamen', 0)
        ax.set_extent(self.extent)
        if stamen > 0:
            ax.add_image(stamen_terrain, 8)
        if self.name == "In":
            In_boun_shp = r'D:\python_toolbox\Igismap\India_Boundary.shp'
            In_boun_feature = ShapelyFeature(Reader(In_boun_shp).geometries(),
                                             crs=projection,
                                             edgecolor='k')
            ax.add_feature(In_boun_feature, facecolor="none")
            if add_state_feature:
                In_st_shp = r'D:\python_toolbox\Igismap\Indian_States.shp'
                state_feature = ShapelyFeature(Reader(In_st_shp).geometries(),
                                               crs=projection, edgecolor='k',
                                               alpha=0.3)
                ax.add_feature(state_feature, facecolor="none")
        else:
            ax.coastlines(color='black', linewidth=1,
                          resolution=self.border_res)
            ax.add_feature(cfeature.BORDERS.with_scale(self.border_res),
                           linestyle='-', alpha=.5)
        gl = ax.gridlines(crs=projection, draw_labels=True,
                          linewidth=1, color='gray', alpha=0.5,
                          linestyle='--')
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.top_labels = False
        gl.right_labels = False
        ax.text(0.75, 0.92, datadict['label'], transform=ax.transAxes,
                bbox=dict(facecolor='white', edgecolor='black'))
        if mode == 'scatter':
            scat = ax.scatter(datadict['lon'], datadict['lat'],
                              c=datadict['plotable'], cmap=cmap, s=1,
                              vmin=datadict['vmin'], vmax=datadict['vmax'],
                              alpha=alpha, transform=projection)
        else:
            llons, llats = np.meshgrid(datadict['lon'], datadict['lat'])
            scat = ax.pcolormesh(llons, llats, datadict['plotable'], cmap=cmap,
                                 vmin=datadict['vmin'], vmax=datadict['vmax'],
                                 alpha=alpha, transform=projection)
    #    if idv_cbar==True:
    #        fig.colorbar(scat, cmap=plt.cm.get_cmap(cmap),
    #                     ax=ax, orientation='horizontal', extend='max',
    #                     fraction=0.1, shrink=0.8, pad=0.1)
        return(scat)
