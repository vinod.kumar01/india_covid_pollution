# -*- coding: utf-8 -*-
"""
Created on Thu May 21 11:06:27 2020

@author: Vinod
"""
# %% functions and imports
import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from tools import tracer_prop


# function to load the data for each site
def load_cpcb(filename, sheetname):
    df = pd.read_excel(filename, sheet_name=sheetname,
                       header=1, na_values='None')
    # if header row is not 1
    if 'From Date' not in df.columns:
        h = df.where(df == 'From Date').dropna(how='all').dropna(axis=1)
        h = h.index
        # h is the header row
        df = pd.read_excel(filename, sheet_name=sheetname,
                           header=2+h, na_values='None')
    if len(df) != 0:
        # delete empty columns
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        df['date_time'] = pd.to_datetime(df['From Date'],
                                         format='%d-%m-%Y %H:%M')
        # replace year by 2016, to have an absolute day-month for ts comparison
        df['abs_dt'] = df.apply(lambda x: x['date_time'].replace(year=2016),
                                axis=1)
        df = df.set_index('date_time')
    return df


# define tracer properties
no2 = {'name': 'NO2', 'label': 'NO$_2$', 'unit': 'ppb', 'ylim': [0, 120]}
no = {'name': 'NO', 'label': 'NO', 'unit': 'ppb', 'ylim': [0, 50]}
co = {'name': 'CO', 'label': 'CO', 'unit': 'ppm', 'ylim': [0, 2.5]}
ozone = {'name': 'Ozone', 'label': 'Ozone', 'unit': 'ppb',
         'ylim': [0, 150]}
pm2_5 = {'name': 'PM2.5', 'label': 'PM$_{2.5}$', 'unit': '$\mu$g m$^{-3}$',
         'ylim': [0, 300]}
pm10 = {'name': 'PM10', 'label': 'PM$_{10}$', 'unit': '$\mu$g m$^{-3}$',
        'ylim': [0, 600]}

# %% load data
srcdir = r'D:\postdoc\Collab\MOES\AQ'
years = ['CPCB_2019', 'CPCB_2020']
states = os.listdir(r'D:\postdoc\Collab\MOES\AQ\CPCB_2019')
states = [i.split('.')[0] for i in states]
statelist = ['Punjab']
for state in states:
    if state not in statelist:
        continue
    sites = {}
    tracer_list = {'PM10': pm10, 'PM2.5': pm2_5,
                   'NO': no, 'NO2': no2, 'CO': co,
                   'Ozone': ozone}
    for year in years:
        filename = os.path.join(srcdir, year, state+'.xlsx')
        sites[year] = pd.ExcelFile(filename).sheet_names
    for site in sites[year]:
        # skip the sites which are not not common in 2019 and 2020
        if site in list(set(sites['CPCB_2020']) - set(sites['CPCB_2019'])):
            continue
        # skip default excel sheets
        if 'Sheet' in site:
            continue
    #    if site != sites[year][0]:
    #        continue
        df_2020 = load_cpcb(filename, sheetname=site)
        df_2019 = load_cpcb(filename.replace('CPCB_2020', 'CPCB_2019'),
                            sheetname=site)
# %% plotting
        for tracer, code in tracer_list.items():
            prop = tracer_prop(**code)
            try:
                fig, ax = plt.subplots(figsize=[9, 4])
                ax.plot('abs_dt', tracer, '-+', data=df_2019, label=2019)
                ax.plot('abs_dt', tracer, '-x', data=df_2020, label=2020)
                ax.set_xlim(['03-01-2016'], ['05-10-2016'])
                ax.xaxis.set_major_locator(mdates.DayLocator(interval=10))
                ax.xaxis.set_minor_locator(mdates.DayLocator(interval=2))
                ax.xaxis.set_major_formatter(mdates.DateFormatter("%d-%b"))
                ax.grid(alpha=0.4, axis='y')
                ax.set_ylabel('{} ({})'.format(prop.label, prop.unit))
                ax.set_ylim(prop.ylim)
                ax.legend(loc='upper right')
                ax.text(0.01, 0.85, state+'\n('+site+')',
                        transform=ax.transAxes)
                plt.tight_layout()
                savedir = os.path.join(srcdir, 'plots', state)
                os.makedirs(savedir, exist_ok=True)
                plt.savefig(os.path.join(savedir,
                                         '{}_{}.png'.format(site, tracer)),
                            format='png', dpi=100)
                plt.close()
            except ValueError:
                print('{} not measured for {}'.format(tracer, site))
                plt.close()
