# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 17:20:09 2020

@author: Vinod
"""

import numpy as np
import os
from netCDF4 import Dataset
import yaml
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from plot_tools import map_prop
from mod_colormap import add_white
#from tools import dict2hdf

#new_cm = add_white('Reds', shrink_colormap=True, replace_frac=0.1, start=None)
new_cm = add_white("rainbow", shrink_colormap=False, replace_frac=0.1,
                   start="min")

with open('config/config_roi.cnf') as conf:
    rois = yaml.load(conf)

for roi in rois:
    rois[roi]['extent'] = rois[roi]['lonrange'].copy()
    rois[roi]['extent'].extend(rois[roi]['latrange'])
    rois[roi]['border_res'] = '10m'
state_capitals = {"Amravati": [16.51, 80.51], "New Delhi": [28.61, 77.21],
                  "Itanagar": [27.08, 93.61], "Dispur": [26.14, 91.79],
                  "Patna": [25.59, 85.14], "Raipur": [21.25, 81.63],
                  "Panaji": [15.4, 73.8],  "Gandhinagar": [23.2, 72.64],
                  "Shimla": [31.1, 77.2], "Ranchi": [23.34, 85.3],
                  "Bengaluru": [12.97, 77.59],
                  "Thiruvananthapuram": [8.52, 76.93],
                  "Bhopal": [23.26, 77.41], "Mumbai": [19.08, 72.88],
                  "Imphal": [24.82, 93.94], "Shillong": [25.58, 91.89],
                  "Aizawl": [23.73, 92.72], "Kohima": [25.67, 94.11],
                  "Bhubaneswar": [20.30, 85.82], "Chandigarh": [30.73, 76.78],
                  "Jaipur": [26.91, 75.79], "Gangtok": [27.33, 88.61],
                  "Chennai": [13.08, 80.27], "Hyderabad": [17.38, 78.49],
                  "Agartala": [23.83, 91.29], "Lucknow": [26.85, 80.95],
                  "Dehradun": [30.32, 78.03], "Kolkata": [22.57, 88.36],
                  "Srinagar": [34.08, 74.80], "Leh": [34.15, 77.58]}
keepdir = r'D:\postdoc\Sat\India_grid_mask'
#satdir = r"M:\nobackup\vinod\tropomi_raster\crop\gridded"
satdir = r"M:\nobackup\vinod\MODIS_AOD\raw\l2_1km\India_covid\stitched\gridded\mean"
savedir = r'D:\postdoc\Collab\MOES\Plots'
keeplist = os.listdir(keepdir)
states = np.genfromtxt(r'D:\postdoc\Collab\MOES\state_order.txt',
                       dtype='str')
tracer_name = 'AOD'
phase = 'Phase 3'
tracer = {'AOD': ['AOD (550 nm)',[0.1, 1.0]],
          'no2': ['NO$_2$ TVCD (molecules cm$^{-2}$)', [-3e14, 6e15]],
          'so2': ['SO$_2$ TVCD (molecules cm$^{-2}$)', [-1e16, 3e16]],
          'hcho': ['HCHO (molecules cm$^{-2}$)', [0, 2e16]]}
phase_dir = {'Phase 1': ['25.03.19-14.04.19', '25 Mar. - 14 Apr.'],
             'Phase 2': ['15.04.19-03.05.19', '15 Mar. - 03 May'],
             'Phase 3': ['04.05.19-17.05.19', '04 May - 17 Jun.']}

f1 = Dataset(os.path.join(satdir, tracer_name, 'India',
                          'In_{}_{}.nc'.format(tracer_name, phase_dir[phase][0])))
f2 = Dataset(os.path.join(satdir, tracer_name, 'India',
                          'In_{}_{}.nc'.format(tracer_name,
                                               phase_dir[phase][0].replace('19', '20'))))
lats = f1.variables['lat'][:]
lons = f1.variables['lon'][:]
llons, llats = np.meshgrid(lons, lats)

try:
    mean_2019 = f1.variables['mean'][:]
    mean_2020 = f2.variables['mean'][:]
except KeyError:
    mean_2019 = f1.variables['AOD_550'][:]
    mean_2020 = f2.variables['AOD_550'][:]
diff = mean_2020-mean_2019
#f1.close()
#f2.close()
# %% processing to calculate difference
fig1, ax1 = plt.subplots(figsize=rois[roi]['figsize'],
                         subplot_kw=dict(projection=ccrs.PlateCarree()))
fig2, ax2 = plt.subplots(figsize=rois[roi]['figsize'],
                         subplot_kw=dict(projection=ccrs.PlateCarree()))
fig3, ax3 = plt.subplots(figsize=rois[roi]['figsize'],
                         subplot_kw=dict(projection=ccrs.PlateCarree()))
map2plot = map_prop(rois['India'])
datadict = {'lat': llats,
            'lon': llons,
            'plotable': mean_2019,
            'vmin': tracer[tracer_name][1][0],
            'vmax': tracer[tracer_name][1][1],
            'label': '',
            'text': tracer[tracer_name][0]}
if 'mean_2019_land' not in globals():
    mean_2019_land = {'lat': [], 'lon': [],
                      'Phase 1': [], 'Phase 2': [], 'Phase 3': []}
    mean_2020_land = {'lat': [], 'lon': [],
                      'Phase 1': [], 'Phase 2': [], 'Phase 3': []}
else:
    pass
for i, state in enumerate(states):
    keep = np.loadtxt(os.path.join(keepdir, 'keep_3km_'+state+'.txt'))
    keep = keep.copy().astype(bool)
    mean_2019_sel = mean_2019[keep]
    mean_2019_land[phase].extend(mean_2019_sel)
    mean_2020_sel = mean_2020[keep]
    mean_2020_land[phase].extend(mean_2020_sel)
#    mean_2019_land['lat'].extend(llats[keep])
#    mean_2020_land['lat'].extend(llats[keep])
#    mean_2019_land['lon'].extend(llons[keep])
#    mean_2020_land['lon'].extend(llons[keep])
#    diff_sel = diff[keep]
#    dict_sel = {'lat': llats[keep], 'lon': llons[keep],
#                'plotable': mean_2019_sel}
#    datadict.update(dict_sel)
#    if i == 0:
#        scat1 = map2plot.plot_map(datadict, ax1, cmap=new_cm, mode='scatter',
#                                  alpha=0.9, add_state_feature=True, stamen=8)
#        datadict['plotable'] = mean_2020_sel
#        scat2 = map2plot.plot_map(datadict, ax2, cmap=new_cm, mode='scatter',
#                                  alpha=0.9, add_state_feature=True, stamen=8)
#        datadict.update({'plotable': diff_sel, 'vmin': -0.2*datadict['vmax'],
#                         'vmax': 0.2*datadict['vmax']})
#        scat3 = map2plot.plot_map(datadict, ax3, cmap='RdBu_r', mode='scatter',
#                                  alpha=0.9, add_state_feature=True, stamen=8)
#    else:
#        datadict.update({'plotable': mean_2019_sel,
#                         'vmin': tracer[tracer_name][1][0],
#                         'vmax': tracer[tracer_name][1][1]})
#        ax1.scatter(datadict['lon'], datadict['lat'],
#                    c=datadict['plotable'], cmap=new_cm, s=1,
#                    vmin=datadict['vmin'], vmax=datadict['vmax'],
#                    alpha=0.9, transform=ccrs.PlateCarree())
#
#        datadict['plotable'] = mean_2020_sel
#        ax2.scatter(datadict['lon'], datadict['lat'],
#                    c=datadict['plotable'], cmap=new_cm, s=1,
#                    vmin=datadict['vmin'], vmax=datadict['vmax'],
#                    alpha=0.9, transform=ccrs.PlateCarree())
#
#        datadict.update({'plotable': diff_sel, 'vmin': -0.2*datadict['vmax'],
#                         'vmax': 0.2*datadict['vmax']})
#        ax3.scatter(datadict['lon'], datadict['lat'],
#                    c=datadict['plotable'], cmap='RdBu_r', s=1,
#                    vmin=datadict['vmin'], vmax=datadict['vmax'],
#                    alpha=0.9, transform=ccrs.PlateCarree())
#ax1.text(0.7, 0.85, '2019\n'+phase+'\n'+phase_dir[phase][1],
#         transform=ax1.transAxes, size=13, bbox=dict(facecolor='white',
#                                                     edgecolor='black'))
#cb1 = fig1.colorbar(scat1, ax=ax1, orientation='horizontal',
#                  extend='max', fraction=0.05, shrink=0.7, pad=0.07)
#cb1.set_label(datadict['text'], size=13)
#fig1.savefig(os.path.join(savedir,
#                          'Map_2019_{}_{}.png'.format(tracer_name,
#                                                      phase.replace(' ', '_'))),
#             format='png', dpi=300)
#plt.close(fig1)
#
#ax2.text(0.7, 0.85, '2020\n'+phase+'\n'+phase_dir[phase][1],
#         transform=ax2.transAxes, size=13, bbox=dict(facecolor='white',
#                                                     edgecolor='black'))
#cb2 = fig2.colorbar(scat2, ax=ax2, orientation='horizontal',
#                  extend='max', fraction=0.05, shrink=0.7, pad=0.07)
#cb2.set_label(datadict['text'], size=13)
#fig2.savefig(os.path.join(savedir,
#                         'Map_2020_{}_{}.png'.format(tracer_name,
#                                                     phase.replace(' ', '_'))),
#            format='png', dpi=300)
#plt.close(fig2)
#
#ax3.text(0.7, 0.85, '2020 - 2019\n'+phase+'\n'+phase_dir[phase][1],
#         transform=ax3.transAxes, size=13, bbox=dict(facecolor='white',
#                                                     edgecolor='black'))
#cb3 = fig3.colorbar(scat3, ax=ax3, orientation='horizontal',
#                    extend='both', fraction=0.05, shrink=0.7, pad=0.07)
#cb3.set_label('Change in '+datadict['text'], size=13)
#a = {}
#for i, city in enumerate(state_capitals):
#    a[i] = ax3.scatter(state_capitals[city][1], state_capitals[city][0],
#                       facecolors='none', edgecolors='r')
#fig3.savefig(os.path.join(savedir,
#                          'diff_{}_{}.png'.format(tracer_name,
#                                                  phase.replace(' ', '_'))),
#             format='png', dpi=300)
