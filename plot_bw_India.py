# -*- coding: utf-8 -*-
"""
Created on Wed Jun 24 12:39:47 2020

@author: Vinod
"""

from tools import nc2dict
import matplotlib.pyplot as plt
import numpy as np
from os import path

dirname = r'D:\postdoc\Collab\MOES'

tracer_name = 'AOD'
tracer = {'AOD': ['AOD (550 nm)', [0, 1]],
          'no2': ['NO$_2$ TVCD (molecules cm$^{-2}$)', [0, 4e15]],
          'so2': ['SO$_2$ TVCD (molecules cm$^{-2}$)', [-1e16, 3e16]],
          'hcho': ['HCHO (molecules cm$^{-2}$)', [0, 2e16]]}
filename_2019 = tracer_name+'_2019_India_land.hdf'
filename_2020 = tracer_name+'_2020_India_land.hdf'
data_2019 = nc2dict(path.join(dirname, filename_2019))
data_2020 = nc2dict(path.join(dirname, filename_2020))

c = 'r'

bw = {'boxprops': {'color': c}, 'whiskerprops': {'color': c},
      'capprops': {'color': c}, 'medianprops': {'color': c},
      'meanprops': {'marker': 'o', 'markeredgecolor': c,
                    'markerfacecolor': 'none', 'markersize': 16}}
fig, ax = plt.subplots(figsize=[8, 4])
box_data_2019 = []
box_data_2020 = []
phases = ['Phase 1', 'Phase 2', 'Phase 3']
rel_diff = {}
for i, phase in enumerate(phases):
    x1 = np.array(data_2019[phase])
    x2 = np.array(data_2020[phase])
    box_data_2019.append(x1[~np.isnan(x1)])
    box_data_2020.append(x2[~np.isnan(x2)])
    rel_diff[phase] = np.nanmean(x2)-np.nanmean(x1)
    rel_diff[phase] *= 100/np.nanmean(x1)
ax.boxplot(box_data_2019, positions=np.arange(3)+0.8,
           showfliers=False, showmeans=True, widths=0.3,
           boxprops=bw['boxprops'], medianprops=bw['medianprops'],
           capprops=bw['capprops'],
           meanprops=bw['meanprops'], whiskerprops=bw['whiskerprops'])
for key in bw:
    bw[key]['color'] = 'b'
    bw[key]['markeredgecolor'] = 'b'
ax.boxplot(box_data_2020, positions=np.arange(3)+1.2,
           showfliers=False, showmeans=True, widths=0.3,
           boxprops=bw['boxprops'], medianprops=bw['medianprops'],
           capprops=bw['capprops'],
           meanprops=bw['meanprops'], whiskerprops=bw['whiskerprops'])
ax.set_xticks(np.arange(3)+1)
ax.set_xticklabels(phases)
ax.set_ylabel(tracer[tracer_name][0], size=14)
ax.set_ylim(tracer[tracer_name][1])
ax.grid(alpha=0.4, axis='y')
ax.scatter(1, -1e20, facecolor='none', edgecolor='r', label='2019')
ax.scatter(1, -1e20, facecolor='none', edgecolor='b', label='2020')
ax.legend(loc='upper right')
plt.tight_layout()
for i, v in enumerate(rel_diff):
    ax.text(i+0.86, ax.yaxis.get_ticklocs()[-3],
            '{:.1f}%'.format(rel_diff[v]), color='k', size=14)
plt.savefig(path.join(dirname, 'boxplot_{}_India.png'.format(tracer_name)),
            format='png', dpi=300)
