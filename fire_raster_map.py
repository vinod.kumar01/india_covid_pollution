'''
Created on 13-Nov-2018

@author: Vinod
'''
import pandas as pd
import numpy as np
import os
from glob import glob
import cartopy as ccrs
import matplotlib.pyplot as plt
import yaml
from mod_colormap import add_white
from plot_tools import map_prop
from datetime import date


with open('config/config_roi.cnf') as conf:
    rois = yaml.load(conf)
for roi in rois:
    rois[roi]['extent'] = rois[roi]['lonrange'].copy()
    rois[roi]['extent'].extend(rois[roi]['latrange'])
    rois[roi]['border_res'] = '10m'
roi = 'India'
start_date = pd.to_datetime('04-05-2019', format='%d-%m-%Y')
end_date = pd.to_datetime('17-05-2019', format='%d-%m-%Y')
year = str(start_date.year)
dirname = r'D:\postdoc\Collab\MOES\MODIS_fire'
new_cm = add_white('Reds', shrink_colormap=True, replace_frac=0.1, start=None)
filename = 'fire_*.csv'
in_file = glob(os.path.join(dirname, year, filename))[0]
df = pd.read_csv(in_file, dtype=None, delimiter=',', header=0)
if "fire_archive_V1" in filename:
    df = df[df['confidence'] == "h"]
else:
    confidence = 50
    df = df[df['confidence'] >= confidence]
df['Date_time'] = df.apply(lambda x: x['acq_date']+str(x['acq_time']).zfill(4),
                           axis=1)
df['Date_time'] = pd.to_datetime(df['Date_time'], format='%Y-%m-%d%H%M')
map_legend = start_date.strftime('%d.%m.%y')+'-'+end_date.strftime('%d.%m.%y')
idx = (df['Date_time'] >= start_date) & (df['Date_time'] < end_date)
df_sel = df[idx]
param = 'firecount'   # brightness,frp,firecount,bright_t31
'''
Define map properties
'''
# Marker locations to add on map
loc = [{'lon': 76.729, 'lat': 30.667, 'site': 'Mohali'}]
#        {'lon': 75.86, 'lat': 30.90, 'site': 'Ludhiana'},
#        {'lon': 76.93, 'lat': 30.83, 'site': 'Kalka'},
#        {'lon': 76.79, 'lat': 30.96, 'site': 'Baddi'},
#        {'lon': 76.37, 'lat': 31.38, 'site': 'Nangal'},
#        {'lon': 77.173, 'lat': 31.105, 'site': 'Shimla'},
#        {'lon': 76.687, 'lat': 31.34, 'site': 'Bilaspur'}]
# map boundaries
# dynamic from data
lat_st = int(np.nanmin(df_sel['latitude'][:]))
lat_end = int(np.nanmax(df_sel['latitude'][:]))
lon_st = int(np.nanmin(df_sel['longitude'][:]))
lon_end = int(np.nanmax(df_sel['longitude'][:]))

## fixed
#lat_st = 28
#lat_end = 34
#lon_st = 73
#lon_end = 80

# grid resolution
resol_lat = 0.25  # degrees
resol_lon = 0.25  # degrees
lat_length = (lat_end-lat_st)/resol_lat
lon_length = (lon_end-lon_st)/resol_lon
rast = np.zeros((int(lat_length), int(lon_length)))
lat = np.zeros((int(lat_length)))
lon = np.zeros((int(lon_length)))
#map_bounds = [lon_st, lon_end, lat_st, lat_end]
#Inp = map_prop(loc, bounds=map_bounds, stamen_level=8)

counter_lat = 0
counter_lon = 0
for lats in np.arange(lat_st, lat_end, resol_lat):
    for lons in np.arange(lon_st, lon_end, resol_lon):
        cond = (df_sel['latitude'] >= lats)
        cond &= (df_sel['latitude'] < (lats + resol_lat))
        cond &= (df_sel['longitude'] >= lons)
        cond &= (df_sel['longitude'] < (lons + resol_lon))
        if param == 'firecount':
            rast[counter_lat, counter_lon] = len(df_sel[cond])
        else:
            rast[counter_lat, counter_lon] = np.nanmean(df_sel[cond][param])
        lon[counter_lon] = lons
        counter_lon = counter_lon+1
    lat[counter_lat] = lats
    counter_lat = counter_lat+1
    counter_lon = 0
'''
plotting
'''
map2plot = map_prop(rois[roi])
datadict = {'lat': lat, 'lon': lon, 'plotable': rast,
            'vmin': 0, 'vmax': 75, 'label': map_legend,
            'text': 'MODIS fire counts (> 50% confidence)'}
fig, ax = plt.subplots(figsize=rois[roi]['figsize'],
                       subplot_kw=dict(projection=ccrs.crs.PlateCarree()))
scat = map2plot.plot_map(datadict, ax, cmap=new_cm)
cb = fig.colorbar(scat, ax=ax, orientation='horizontal',
                  extend='max', fraction=0.05, shrink=0.7, pad=0.07)
cb.set_label(datadict['text'])
plt.savefig(os.path.join(dirname, 'fire_'+map_legend+'.png'),
            format='png', dpi=300)
