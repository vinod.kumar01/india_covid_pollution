# -*- coding: utf-8 -*-
"""
Created on Thu May 21 12:44:54 2020

@author: Vinod
"""
import netCDF4
import h5py


class tracer_prop:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.label = kwargs.get('label')
        self.unit = kwargs.get('unit')
        self.ylim = kwargs.get('ylim')


def dict2nc1d(fname, dict_out, mode='w'):
    with netCDF4.Dataset(fname, mode) as nc:
        nc.createDimension('dim', len(next(iter(dict_out.values()))))
        for key in dict_out:
            try:
                x = nc.createVariable(key, 'f4', 'dim')
                x[:] = dict_out[key]
            except:
                print(key, "not written to output")


def dict2hdf(fname, dict_out, mode='w'):
    with h5py.File(fname, mode) as hf:
        for key in dict_out:
            try:
                hf.create_dataset(key, data=dict_out[key])
            except:
                print(key, "not written to output")


def nc2dict(fname):
    # convert normal nc files to dictionary
    data = netCDF4.Dataset(fname, "r")
    dct = {key: data[key][:] for key in data.variables.keys()}
    data.close()
    return(dct)
