# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 00:15:38 2020

@author: Vinod
"""
import os
import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np

def value2color(value, vmin, vmax, cmap="jet"):
    #normalize item number values to colormap
    norm = colors.Normalize(vmin=vmin, vmax=vmax)   
    #colormap possible values = viridis, jet, spectral
    colortable = plt.cm.get_cmap(cmap, 256)
    color = colortable(norm(value))
    return color


def add_white(colormap, shrink_colormap=True, replace_frac=0.1, start=None):
    '''
    @colormap: python colormap to modify- string
    @shrink_colormap: Whether to shrink the colormap or cut the colormap from
                        beginning
    @ replace_frac: Fraction of colormap to replace by white
    @start: whether to start from zero or minimum of colormap. use "min" for
    minimum
    '''
    colortable = plt.cm.get_cmap(colormap, 256)
    white = np.array([1, 1, 1, 0.5])
    if start=="min":
        white = colortable(replace_frac)   
    newcolors = colortable(np.linspace(0, 1, 256))
    replace_vals = int(256*replace_frac)
    ## add decreasing transparency to the lower panels of uniform color
    ## minimum transparency set to 0.4
    replace_color = np.tile(white, [replace_vals,1])
    for i in range(replace_vals):
        replace_color[i, 3] = max(0.4, i/replace_vals) if i/replace_vals<0.4 else i/replace_vals
    ##
    newcolors[:replace_vals, :] = replace_color
    if shrink_colormap:
        shrink_color = colortable(np.linspace(0, 1, 256-replace_vals))   
        newcolors[replace_vals:, :] = shrink_color
    newcmp = colors.ListedColormap(newcolors)
    return newcmp


def man_cm(colormap, cut_init=None):
    dirname = r'D:\python_toolbox\cmaps'
    cmap_list = np.genfromtxt(os.path.join(dirname, colormap+".dat"))
    cmap_list= cmap_list[cut_init:]
    newcmp = colors.LinearSegmentedColormap.from_list(colormap, cmap_list)
    return newcmp