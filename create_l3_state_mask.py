# -*- coding: utf-8 -*-
"""
Created on Fri May 22 00:05:44 2020

@author: Vinod
"""
import os
import numpy as np
import netCDF4
import shapefile
from shapely.geometry import shape, Point
import time


def check(polygon, lon, lat):
    # build a shapely point from your geopoint
    point = Point(lon, lat)
    # the contains function does exactly what you want
    return polygon.contains(point)


filename = r'M:\nobackup\vinod\tropomi_raster\raw\NO2\mon_mean\no2_201802.nc'
destdir = r'D:\postdoc\Sat\India_grid_mask\tropomi_l3'
with netCDF4.Dataset(filename) as f:
    latgrid = f.variables['lat'][:]
    longrid = f.variables['lon'][:]
xygrid = np.meshgrid(longrid, latgrid)
lons = xygrid[0]
lats = xygrid[1]
In_st_shp = r'D:\python_toolbox\Igismap\Indian_States.shp'
r = shapefile.Reader(In_st_shp)
shapeRecs = r.shapeRecords()
shapes = r.shapes()
states = {}

# %%
for i in range(len(shapes)):
    state_short = shapeRecs[i].record[0].split(' ')[0]
    state_name = shapeRecs[i].record[0]
    print(state_short)
    states[shapeRecs[i].record[0]] = shapes[i]
    polygon = shape(shapes[i])
    minx, miny, maxx, maxy = polygon.bounds
    keep = np.zeros(lats.shape, dtype=bool)
    t = time.time()
    for i in range(len(latgrid)):
        for j in range(len(longrid)):
            if (miny < lats[i, j] < maxy) & (minx < lons[i, j] < maxx):
                keep[i, j] = check(polygon, lons[i, j], lats[i, j])
#                print('{},{}'.format(i, j))
    keep_bin = keep.copy().astype(int)
    outname = os.path.join(destdir, 'keep_3km_' + state_short + '.txt')
    np.savetxt(outname, keep_bin, fmt='%i')
    elapsed = (time.time() - t)/60
    print('{} minutes elapsed for {}'.format(elapsed, state_short))
